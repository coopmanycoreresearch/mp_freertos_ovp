#!/usr/bin/env bash
export PROJECT_FOLDER="$(pwd)"
export OUTPUT_FOLDER="${PROJECT_FOLDER}/simulation-output"
export FREERTOS_FOLDER="${PROJECT_FOLDER}/freertos"
export PLATFORM_FOLDER="${PROJECT_FOLDER}/platform"
export EXTENSION_FOLDER="${PROJECT_FOLDER}/extension"
export CMSIS_FOLDER="${PROJECT_FOLDER}/cmsis"
export APPLICATOIONS_FOLDER="${PROJECT_FOLDER}/applications"
TESTCASE_FILE="${PROJECT_FOLDER}/testcases.txt"
RESULTS_FOLDER="${PROJECT_FOLDER}/simulation-log"
SCRIPT_FILE="run.sh"
HOST_PROCESSORS=$(grep -c ^processor /proc/cpuinfo)
USE_PROCESSORS=$((${HOST_PROCESSORS}/4))
TIME_OUT="timeout 40s"

#generate_testcases()
#{
    #if [[ ! -f ${TESTCASE_FILE} ]]
    #then

        #mapping_list="LECDN NN"
        #processor_list="M3 M4F"

        #app_list="
        #prod_cons:prod_cons=1
        #mpeg:mpeg=1
        #dtw:dtw=1
        #djk:dijkstra=1
        #"
        #noc_list="2"
        #cluster_list="2"
        #tasks_list="2"
        #generate_testcase_file

    #fi
#}

generate_testcases()
{
    if [[ ! -f ${TESTCASE_FILE} ]]
    then

        mapping_list="LECDN"
        processor_list="M4F"

        app_list="
        prod_cons:prod_cons=1
        "
        noc_list="2"
        cluster_list="2"
        tasks_list="2"
        generate_testcase_file

    fi
}

generate_testcase_file()
{
    local i=0
    for processor in ${processor_list}
    do
        for mapping in ${mapping_list}
        do
            for app in ${app_list}
            do
                IFS=':' read -r -a app_args <<< "${app}"
                for noc in ${noc_list}
                do
                    for cluster in ${cluster_list}
                    do
                        for tasks in ${tasks_list}
                        do
                            echo -e "--name ${app_args[0]} --applications \"${app_args[@]:1}\" --noc ${noc} ${noc} --cluster ${cluster} ${cluster} --tasks ${tasks} --mapping ${mapping} --processor ${processor} --premap 0 --buffer 8 --routing XY --global 0 --watchdog 0 --uart 1 --rom 1 --mips 1 --ovp-dbg 1 --dma 1 --memory-access 1 --simulation-type OVP --nodes 1 --noc-enable 1">>${TESTCASE_FILE}
                            #echo -e "--name ${app_args[0]} --applications \"${app_args[@]:1}\" --noc ${noc} ${noc} --cluster ${cluster} ${cluster} --tasks ${tasks} --mapping ${mapping} --processor ${processor} --premap 1 --buffer 8 --routing XY --global 0 --watchdog 0 --uart 1 --rom 1 --mips 1 --ovp-dbg 1 --dma 1 --memory-access 1 --simulation-type OVP --nodes 1 --noc-enable 1">>${TESTCASE_FILE}
                        done
                    done
                done
                i=$(( ${i}+1 ))
            done
        done
    done
}

config_tescase()
{
    while [ $# -gt 0 ]
    do
        key="${1}"
        case ${key} in
            "--simulation"     ) SIMULATION_OPTIONS="${2}"    ; ;;
            "--make"           ) MAKE_COMMON="${2}"           ; ;;
            "--make-free"      ) MAKE_FREERTOS="${2}"         ; ;;
            "--make-app"       ) MAKE_APPLICATIONS="${2}"     ; ;;
            "--make-platform"  ) MAKE_PLATFORM="${2}"         ; ;;
            "--name"           ) PROJECT_NAME="${2}"          ; ;;
            "--tasks"          ) export MAX_TASKS="${2}"      ; ;;
            "--processor"      ) export PROCESSOR="${2}"      ; ;;
            "--buffer"         ) export NB="${2}"             ; ;;
            "--mapping"        ) export MAPPING="${2}"        ; ;;
            "--noc"            ) export CPUS_X="${2}"         ;
                                 export CPUS_Y="${3}"         ; shift ;;
            "--cluster"        ) export WIDTH_CLUSTER="${2}"  ;
                                 export HEIGHT_CLUSTER="${3}" ; shift ;;
            "--routing"        ) ROUTING="${2}"               ; ;;
            "--global"         ) export G_MASTER="${2}"       ; ;;
            "--applications"   ) set_applications "${2}"      ; ;;
            "--watchdog"       ) export watchdog="${2}"       ; ;;
            "--uart"           ) export UART="${2}"           ; ;;
            "--rom"            ) export ROM="${2}"            ; ;;
            "--mips"           ) export MIPS="${2}"           ; ;;
            "--ovp-dbg"        ) export OVP_DBG="${2}"        ; ;;
            "--dma"            ) export USE_DMA="${2}"        ; ;;
            "--premap"         ) export PREMAP="${2}"         ; ;;
            "--memory-access"  ) export MEMORY_ACCESS="${2}"  ; ;;
            "--simulation-type") export SIMULATION="${2}"     ; ;;
            "--nodes"          ) export NODES="${2}"          ; ;;
            "--noc-enable"     ) export NoC="${2}"            ; ;;
        esac
        shift 2
    done

    export PCORE=Cortex-${PROCESSOR}
    export CPUS=$((${CPUS_X}*${CPUS_Y}))
    export N_CLUSTER=$(((${CPUS_X}*${CPUS_Y}) / (${HEIGHT_CLUSTER}*${WIDTH_CLUSTER})))
    export MAX_P=$(((${CPUS_X}*${CPUS_Y})-${N_CLUSTER}))
    export MAX_THREADS=$((((${CPUS_X}*${CPUS_Y})-${N_CLUSTER})*($MAX_TASKS)))
    export TESTCASE_CONFIG=noc-${CPUS_X}x${CPUS_Y}-clusters-${N_CLUSTER}-tasks-${MAX_TASKS}-${MAPPING}-${PREMAP}-${PROCESSOR}-${PROJECT_NAME}
    export CURRENT_TESTCASE_FOLDER=${OUTPUT_FOLDER}/${TESTCASE_CONFIG}

    mkdir -p \
        ${CURRENT_TESTCASE_FOLDER}/log \
        ${CURRENT_TESTCASE_FOLDER}/repository \
        ${CURRENT_TESTCASE_FOLDER}/debug/traffic_router

    echo -e "${app_file}" > ${CURRENT_TESTCASE_FOLDER}/testcase.cfg
    export APP_NUMBER=$(cat ${CURRENT_TESTCASE_FOLDER}/testcase.cfg | wc -l)

    cat >${CURRENT_TESTCASE_FOLDER}/debug/platform.cfg <<CONFIG_FILE
router_addressing ${ROUTING}
channel_number 1
mpsoc_x ${CPUS_X}
mpsoc_y ${CPUS_Y}
cluster_x ${WIDTH_CLUSTER}
cluster_y ${HEIGHT_CLUSTER}
manager_position_x ${G_MASTER}
manager_position_y ${G_MASTER}
global_manager_cluster ${G_MASTER}
flit_size 32
clock_period_ns 10
BEGIN_task_name_relation
CONFIG_FILE
}

set_applications()
{
    IFS=',' read -r -a list <<< "${1}"
    first=0
    for index in "${!list[@]}"
    do
        IFS='=' read -r -a app <<< $(sed -e 's/^"//' -e 's/"$//' <<<"${list[index]}")
        for i in $(seq 1 "${app[1]}")
        do
            if [[ "${first}" -eq "0" ]]
            then
                first=1
                app_file="${app[0]}"
            else
                app_file="${app_file}\n${app[0]}"
            fi
        done
    done
}

compile()
{
    generate_testcases

    rm -rf ${OUTPUT_FOLDER}

    IFS=$'\n'
    for line in $(<${TESTCASE_FILE})
    do
        IFS=' ' read -r -a args <<< "${line}"
        config_tescase "${args[@]}"
        OUTPUT="| tee -a ${CURRENT_TESTCASE_FOLDER}/output.log"

        eval compile_applications       "${OUTPUT}"
        eval compile_platform           "${OUTPUT}"
        eval compile_freertos           "${OUTPUT}"
        eval generate_simulation_script "${OUTPUT}"
    done
}

compile_applications()
{
    printf "\n\t /**************** Application Compilation ****************/ \n\n"

    cd ${APPLICATOIONS_FOLDER}

    g++ -O0 -o parser parser.cpp

    local current_app=""
    local num_tasks=1
    local apps_type=0

    for current_app in $(cat ${CURRENT_TESTCASE_FOLDER}/testcase.cfg | sort | uniq) ; do
        printf "\n*** Compiling ${current_app}\n"
        cd ${APPLICATOIONS_FOLDER}/${current_app}
        make clean  ${MAKE_COMMON} ${MAKE_APPLICATIONS} || exit
        make        ${MAKE_COMMON} ${MAKE_APPLICATIONS} || exit
        for task in $(cat ${current_app}.tsk) ; do
            paths=${paths}"./"${current_app}"/"${task}"\n"
            task_id=$((($apps_type * 256)+${num_tasks}))
            echo -e "${task%.lst} ${task_id}" >> ${CURRENT_TESTCASE_FOLDER}/debug/platform.cfg
            num_tasks=$((${num_tasks}+1))
        done
        echo -e "${paths}" > apps.tsk
        apps_type=$(($apps_type+1))
        cp -l -r ${APPLICATOIONS_FOLDER}/${current_app} ${CURRENT_TESTCASE_FOLDER}/${current_app}
    done

    cd ${APPLICATOIONS_FOLDER}
    ./parser \
        ./ \
        "$(cat ${CURRENT_TESTCASE_FOLDER}/testcase.cfg | wc -l)" \
        ${CURRENT_TESTCASE_FOLDER}/testcase.cfg

    cp ./repository.h ${CURRENT_TESTCASE_FOLDER}/repository

    echo -e "END_task_name_relation" >> ${CURRENT_TESTCASE_FOLDER}/debug/platform.cfg

    for current_app in $(cat ${CURRENT_TESTCASE_FOLDER}/testcase.cfg | sort | uniq) ; do
        cd ${APPLICATOIONS_FOLDER}/${current_app}
        make clean ${MAKE_COMMON} ${MAKE_APPLICATIONS} || exit
    done

}

compile_platform()
{
    printf "\n\t /***************** Platform Generation *****************/ \n\n"
    cd ${PLATFORM_FOLDER}

    # make clean ${MAKE_COMMON} ${MAKE_PLATFORM} -C ./repository  || exit
    # make all   ${MAKE_COMMON} ${MAKE_PLATFORM} -C ./repository  || exit

    make clean ${MAKE_COMMON} ${MAKE_PLATFORM} -C ./router      || exit
    make all   ${MAKE_COMMON} ${MAKE_PLATFORM} -C ./router      || exit

    make clean ${MAKE_COMMON} ${MAKE_PLATFORM} -C ./module      || exit
    make       ${MAKE_COMMON} ${MAKE_PLATFORM} -C ./module      || exit

    make clean ${MAKE_COMMON} ${MAKE_PLATFORM} -C ./harness     || exit
    make       ${MAKE_COMMON} ${MAKE_PLATFORM} -C ./harness     || exit

    cp router/pse.pse                       ${CURRENT_TESTCASE_FOLDER}/NoC.pse
    cp repository/pse.pse                   ${CURRENT_TESTCASE_FOLDER}/Repository.pse
    cp module/model.${IMPERAS_ARCH}.so      ${CURRENT_TESTCASE_FOLDER}
    cp harness/harness.${IMPERAS_ARCH}.exe  ${CURRENT_TESTCASE_FOLDER}

    cp -l -r ${PLATFORM_FOLDER} ${CURRENT_TESTCASE_FOLDER}/platform
}

compile_freertos()
{
    printf "\n\t /***************** FreeRTOS Generation *****************/ \n\n"

    cd ${FREERTOS_FOLDER}

    make clean ${MAKE_COMMON} ${MAKE_FREERTOS} || exit
    make       ${MAKE_COMMON} ${MAKE_FREERTOS} || exit

    mv FreeRTOSDemo.ARM_CORTEX_${PROCESSOR}.* ${CURRENT_TESTCASE_FOLDER}
    mv *.lst ${CURRENT_TESTCASE_FOLDER}

    cp -l -r ${EXTENSION_FOLDER} ${CURRENT_TESTCASE_FOLDER}/extension
    cp -l -r ${FREERTOS_FOLDER} ${CURRENT_TESTCASE_FOLDER}/freertos
    rm -Rf ${FREERTOS_FOLDER}/Build
}

generate_simulation_script()
{
    printf "\n\t /************* Generating Simulation Script *************/ \n\n"

    cd ${CURRENT_TESTCASE_FOLDER}

    touch ${SCRIPT_FILE}

    chmod +x ${SCRIPT_FILE}

    cat > ${SCRIPT_FILE} <<RUN_SCRIPT
#!/usr/bin/env bash
cd ${CURRENT_TESTCASE_FOLDER}
${TIME_OUT} ./harness.${IMPERAS_ARCH}.exe --quantum 0.001 \\
    --variant Cortex-${PROCESSOR} --numberofcpus ${CPUS}  \\
    --program ./FreeRTOSDemo.ARM_CORTEX_${PROCESSOR}.elf  \\
    --verbose                                             \\
    ${SIMULATION_OPTIONS}                                 \\
&> ./full-log-${TESTCASE_CONFIG}.log

exit_status=\$?
if [[ \$exit_status -ne 0 ]]
then

    echo "Simulation exit with non 0 status" >> ./full-log-${TESTCASE_CONFIG}.log

    if [[ \$exit_status -eq 124 ]]
    then
        echo "Simulation exceeded maximum time" >> ./full-log-${TESTCASE_CONFIG}.log
    fi

fi
RUN_SCRIPT
}

simulate()
{
    ls ${OUTPUT_FOLDER} | xargs -i --max-procs=${USE_PROCESSORS} \
        bash -c "${OUTPUT_FOLDER}/{}/${SCRIPT_FILE}"
    # TODO create_log
    parse_log
}

create_log()
{
    cd ${PROJECT_FOLDER}
    g++ -O0 -o ${OUTPUT_FOLDER}/merge ./merge_log_traffic.cpp

    local files_to_merge=$(ls ${CURRENT_TESTCASE_FOLDER}/debug/traffic_router/*.txt)
    local logs_merge=""
    local count=0
    echo -e "This is the files: "
    for file in ${files_to_merge}
    do
        packets=$(cat ${file})
        contagem=$(cat ${file} | wc -l)
        count=$((${count} + ${contagem}))
        echo -e "${packets}" >> ${CURRENT_TESTCASE_FOLDER}/debug/packets.txt
    done
    printf "${count}\n"
    ./merge  ${count} \
        ${CURRENT_TESTCASE_FOLDER}/debug/packets.txt \
        ${CURRENT_TESTCASE_FOLDER}/debug/traffic_router.txt
}

parse_log()
{

    rm -rf ${RESULTS_FOLDER}

    local ext=""

    for file in $(find ${OUTPUT_FOLDER} -name "full*.log*")
    do

        if [[ $(grep -i "END SIMULATION" ${file}) ]]
        then
            ext="ok"
        else
            ext="hang"
        fi

        if [[ $(grep -i "Simulation exit with non 0 status" ${file}) ]]
        then
            ext="${ext}.killed"
        fi

        if [[ $(grep -i "Simulation exceeded maximum time" ${file}) ]]
        then
            ext="${ext}.timeout"
        fi

        if [[ $(grep -i "License not available" ${file}) ]]
        then
            ext="${ext}.licence"
        fi

        if [[ $(grep -i "Fault" ${file}) ]]
        then
            ext="${ext}.fault"
        fi

        if [[ $(grep -i "Malloc" ${file}) ]]
        then
            ext="${ext}.malloc"
        fi

        mkdir -p ${RESULTS_FOLDER}/${ext}

        cp ${file} ${RESULTS_FOLDER}/${ext}

    done
    unset ext
}

main()
{
    compile
    simulate
}

if [[ $# -eq 0 ]]
then
    main
else
    "${@}"
fi
wait
