## NoC-based Multiprocessor Platform Management with FreeRTOS

---

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**
Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Cloning allows you to work on your files locally, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).
Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. 
You can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

---

## REQUIREMENTS

---
This version requires the imperas commercial license and [GCC](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-a/downloads) compiler.

---

## DEPENDENCIES

---

To supply all software compilation dependencies just run the "install_dependencies.sh" script.


---
## `Applications` folder 

---

Contains the applications source code and makefiles to compile and execute.

---

## `CMSIS` folder 

---

A [CMSIS](https://github.com/ARM-software/CMSIS_5) library submodule.

---

## `Extension` folder 

---

This directory contains the libraries developed to support multiprocessor resource management and task mapping, targeting both generic NN and LECDN mapping implementations. Such library are depends on the CMSIS library.

---

## `FreeRTOS` folder 

---

Contains the standard kernels provided by the [FreeRTOS](https://www.freertos.org/). Note that at this moment our project supports the FreeRTOS kernel v10.2.

---

## `Platform` folder 

---

Contains the Multiprocessor platform developed with Imperas libraries to run over the OVPsim.

---

## `Simulation` 

---

The file "ovp_simulation.sh" contains the configuration parameters used to compile and run an application over the FreeRTOS extension within the Multiprocessor platform.
Note that you must consider the number of tasks of an application to define the platform size. 
For instance, the application prod_cons consists of 2 tasks, thus a 2x2 platform with single thread supports its execution.

---

## `Supported configurations` 

---

'Arm Cortex-M Processors'  M0, M3, M4F
'Applications' prod_cons, mpeg, dtw, dijkstra
'Mapping techniques' NN, LECDN

---

## Paper

---

Please, cite the following paper if you are going to use this extension.

- **FreeRTOS Extension**
  *Abich, G., Mandelli, M. G., Rosa, F. R., Moraes, F., Ost, L., & Reis, R. Extending FreeRTOS to support dynamic and distributed mapping in multiprocessor systems. In 2016 IEEE International Conference on Electronics, Circuits and Systems (ICECS) (pp. 712-715). [10.1109/ICECS.2016.7841301](https://doi.org/10.1109/ICECS.2016.7841301) (2016, December).*
  ```
  @INPROCEEDINGS{abich:16extension,
      author={Abich, G. and Mandelli, M. G. and Rosa, F. R. and Moraes, F. and Ost, L. and Reis, R.},
      booktitle={2016 IEEE International Conference on Electronics, Circuits and Systems (ICECS)}, 
      title={Extending FreeRTOS to support dynamic and distributed mapping in multiprocessor systems}, 
      year={2016},
      volume={},
      number={},
      pages={712-715},
      doi={10.1109/ICECS.2016.7841301}
  }
  ```

---
