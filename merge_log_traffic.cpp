#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <vector>
#include "../extension/include/services.h"

#define MAX_TAM_LIN 128

using namespace std;

typedef struct {
    int tick;
    int current_address;
    int service;
    int payload;
    int bandwidth_allocation;
    int port;
    int target;
    int task_id;
    int consumer;
} packet;


int main (int argc, char **argv)
{
    FILE *fileptrIN;
    FILE *fileptrOUT;


    char temp[MAX_TAM_LIN];
    char temp_read[MAX_TAM_LIN];

    std::vector<std::string> tokens;

    int num_packets;
    int count=0;
    packet *traffic_router;

    string word;
    std::ostringstream file,filein;

    std::string line,partial;
    num_packets=atoi(argv[1]);
    filein<<argv[2];
    file<<argv[3];

    std::ifstream infile(filein.str().c_str());
    cout<<filein.str().c_str()<<endl;
    cout<<file.str().c_str()<<endl;
    traffic_router = (packet *) malloc(sizeof(packet)*num_packets);

    fileptrOUT = fopen(file.str().c_str(), "w");

    while (std::getline(infile, line) && count<=num_packets){
        std::istringstream iss(line);
        std::string token;
        int tmp=0;
        while (std::getline(iss, token, '\t'))   // but we can specify a different one
        {
            int value;
            tokens.push_back(token);
            value=atoi(token.c_str());
            //cout <<token<<" count "<<count<<endl ;
            switch (tmp){
                case 0:{
                           traffic_router[count].tick=value;
                           break;}
                case 1:{
                           traffic_router[count].current_address=value;
                           break;}
                case 2:{
                           traffic_router[count].service=value;
                           break;}
                case 3:{
                           traffic_router[count].payload=value;
                           break;}
                case 4:{
                           traffic_router[count].bandwidth_allocation=value;
                           break;}
                case 5:{
                           traffic_router[count].port=value;
                           break;}
                case 6:{
                           traffic_router[count].target=value;
                           break;}
                case 7:{
                           traffic_router[count].task_id=value;
                           break;}
                case 8:{
                           traffic_router[count].consumer=value;
                           break;}
                default:{
                            cout<<"ERROR CASE NOT RECOGNIZED"<<endl;
                            break;}

            }
            tmp++;
        }
        count++;
    }
    cout<<"Numero de pacotes "<<count<<endl;
    count =0;
    int count_t=0;
    packet aux;
    int index_min;
    while (count<num_packets){
        count_t=count+1;
        index_min=count;
        while (count_t<num_packets){
            if (traffic_router[index_min].tick>traffic_router[count_t].tick){
                index_min=count_t;
            }
            count_t++;
        }
        if (index_min!=count){
            aux.tick=traffic_router[count].tick;
            aux.current_address=traffic_router[count].current_address;
            aux.service=traffic_router[count].service;
            aux.payload=traffic_router[count].payload;
            aux.bandwidth_allocation=traffic_router[count].bandwidth_allocation;
            aux.port=traffic_router[count].port;
            aux.target=traffic_router[count].target;
            aux.task_id=traffic_router[count].task_id;
            aux.consumer=traffic_router[count].consumer;

            traffic_router[count].tick=traffic_router[index_min].tick;
            traffic_router[count].current_address=traffic_router[index_min].current_address;
            traffic_router[count].service=traffic_router[index_min].service;
            traffic_router[count].payload=traffic_router[index_min].payload;
            traffic_router[count].bandwidth_allocation=traffic_router[index_min].bandwidth_allocation;
            traffic_router[count].port=traffic_router[index_min].port;
            traffic_router[count].target=traffic_router[index_min].target;
            traffic_router[count].task_id=traffic_router[index_min].task_id;
            traffic_router[count].consumer=traffic_router[index_min].consumer;

            traffic_router[index_min].tick=aux.tick;
            traffic_router[index_min].current_address=aux.current_address;
            traffic_router[index_min].service=aux.service;
            traffic_router[index_min].payload=aux.payload;
            traffic_router[index_min].bandwidth_allocation=aux.bandwidth_allocation;
            traffic_router[index_min].port=aux.port;
            traffic_router[index_min].target=aux.target;
            traffic_router[index_min].task_id=aux.task_id;
            traffic_router[index_min].consumer=aux.consumer;

        }
        count++;
    }
    count=0;
    while (count<num_packets){
        if (traffic_router[count].service != NI_HANDLER_TASK_ALLOCATION && traffic_router[count].service != NI_HANDLER_TASK_TERMINATED && traffic_router[count].service != 0x221 && traffic_router[count].service != NI_HANDLER_MESSAGE_REQUEST && traffic_router[count].service != NI_HANDLER_MESSAGE_DELIVERY){
            fprintf(fileptrOUT, "%d\t%d\t%d\t%d\t%d\t%d\t%d\n", traffic_router[count].tick, traffic_router[count].current_address, traffic_router[count].service, traffic_router[count].payload, traffic_router[count].bandwidth_allocation, traffic_router[count].port, traffic_router[count].target);
        } else {
            if (traffic_router[count].service == NI_HANDLER_MESSAGE_REQUEST || traffic_router[count].service == NI_HANDLER_MESSAGE_DELIVERY)
                fprintf(fileptrOUT, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", traffic_router[count].tick, traffic_router[count].current_address, traffic_router[count].service, traffic_router[count].payload, traffic_router[count].bandwidth_allocation, traffic_router[count].port, traffic_router[count].target, traffic_router[count].task_id, traffic_router[count].consumer);
            else
                fprintf(fileptrOUT, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", traffic_router[count].tick, traffic_router[count].current_address, traffic_router[count].service, traffic_router[count].payload, traffic_router[count].bandwidth_allocation, traffic_router[count].port, traffic_router[count].target, traffic_router[count].task_id);
        }/**/
        count++;
    }

    fclose(fileptrOUT);
    return 0;
}
